**Shape calculator**

The sample project for the task #2

**SQL**

Scripts with database schema and according queries for the task #3.

* schema.sql - The script for creating required tables and constraints
* data.sql - The script for pushing sample data
* query.sql - The scripts for querying the required dataset (Product-Category)