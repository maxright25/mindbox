using NUnit.Framework;
using ShapeCalculator.Calculators;
using ShapeCalculator.Shapes;
using System;

namespace Calculators
{
    public class TriangleCalculatorTests
    {
        private TriangleCalculator GetSut()
        {
            return new TriangleCalculator();
        }
        
        [Test]
        public void GetPerimeter_WhenWrongShapeIsPassed_ShouldThrow()
        {
            // Arrange
            var triangle = new Circle(2);
            var sut = GetSut();

            // Act & Assert
            Assert.Throws<ArgumentException>(() => sut.GetPerimeter(triangle));
        }

        [Test]
        public void GetPerimeter_ShouldReturn()
        {
            // Arrange
            var a = 2;
            var b = 2;
            var c = 2;

            var trg = new Triangle(a, b, c);
            var expected = 3;

            var sut = GetSut();

            // Act
            var perimeter = sut.GetPerimeter(trg);

            // Assert
            Assert.AreEqual(expected, perimeter);
        }

        [Test]
        public void GetArea_ShouldReturn()
        {
            // Arrange
            var a = 2;
            var b = 2;
            var c = 2;

            var trg = new Triangle(a, b, c);
            var expected = 1.732051;

            var sut = GetSut();

            // Act
            var area = sut.GetArea(trg);

            // Assert
            Assert.AreEqual(expected, area, 1e-5);
        }

        [Test]
        public void IsRight_WhenTriangleIsRight_ShouldReturnTrue()
        {
            // Arrange
            var a = 3;
            var b = 4;
            var c = 5;

            var trg = new Triangle(a, b, c);

            var sut = GetSut();

            // Act
            var isRight = sut.IsRight(trg);

            // Assert
            Assert.IsTrue(isRight);
        }

        [Test]
        public void IsRight_WhenTriangleIsNotRight_ShouldReturnFalse()
        {
            // Arrange
            var a = 2;
            var b = 2;
            var c = 2;

            var trg = new Triangle(a, b, c);

            var sut = GetSut();

            // Act
            var isRight = sut.IsRight(trg);

            // Assert
            Assert.IsFalse(isRight);
        }
    }
}