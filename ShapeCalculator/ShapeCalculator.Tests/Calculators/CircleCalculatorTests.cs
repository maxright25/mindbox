using NUnit.Framework;
using ShapeCalculator.Calculators;
using ShapeCalculator.Shapes;
using System;

namespace Calculators
{
    public class CircleCalculatorTests
    {
        private CircleCalculator GetSut()
        {
            return new CircleCalculator();
        }

        [Test]
        public void GetPerimeter_WhenWrongShapeIsPassed_ShouldThrow()
        {
            // Arrange
            var triangle = new Triangle(3, 4, 5);
            var sut = GetSut();

            // Act & Assert
            Assert.Throws<ArgumentException>(() => sut.GetPerimeter(triangle));
        }

        [Test]
        public void GetPerimeter_ShouldReturn()
        {
            // Arrange
            var circle = new Circle(1);
            var expected = 6.28;

            var sut = GetSut();

            // Act
            var perimeter = sut.GetPerimeter(circle);

            // Assert
            Assert.AreEqual(expected, perimeter, 1e-2);
        }

        [Test]
        public void GetArea_ShouldReturn()
        {
            // Arrange
            var circle = new Circle(1);
            var expected = 3.14;

            var sut = GetSut();

            // Act
            var area = sut.GetArea(circle);

            // Assert
            Assert.AreEqual(expected, area, 1e-2);
        }
    }
}