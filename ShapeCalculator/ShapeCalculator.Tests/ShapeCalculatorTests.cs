﻿using NUnit.Framework;
using ShapeCalculator.Shapes;
using ShapeCalculator;
using System;
using System.Collections.Generic;
using System.Text;
using ShapeCalculator.Calculators;

namespace Tests
{
    class ShapeCalculatorTests
    {
        private ShapeCalculator.ShapeCalculator GetSut()
        {
            return new ShapeCalculator.ShapeCalculator();
        }

        [Test]
        public void GetArea_ShouldReturn()
        {
            // Arrange
            var circle = new Circle(2);
            var sut = GetSut();

            // Act
            var area = sut.GetArea(circle);

            // Assert
            Assert.IsTrue(area > 0);
        }

        [Test]
        public void GetPerimeter_ShouldReturn()
        {
            // Arrange
            var circle = new Circle(2);
            var sut = GetSut();

            // Act
            var perimeter = sut.GetPerimeter(circle);

            // Assert
            Assert.IsTrue(perimeter > 0);
        }

        [Test]
        public void GetArea_WhenHandlerIsNotRegistered_ShouldThrow()
        {
            // Arrange
            var shape = new MyAwesomeShape();
            var sut = GetSut();

            // Act & Assert
            Assert.Throws<KeyNotFoundException>(() => sut.GetArea(shape));
        }

        [Test]
        public void GetArea_WhenHandlerIsRegistered_ShouldReturn()
        {
            // Arrange
            var shape = new MyAwesomeShape();
            var handler = new MyAwesomeCalculator();
            var sut = GetSut();
            sut.RegisterCustomHandler(typeof(MyAwesomeShape), handler);

            // Act
            var area = sut.GetArea(shape);

            // Assert
            Assert.AreEqual(666, area);
        }

        public class MyAwesomeShape : IShape
        {

        }

        public class MyAwesomeCalculator : BaseCalculator<MyAwesomeShape>
        {
            protected override double GetArea(MyAwesomeShape shape)
            {
                return 666;
            }

            protected override double GetPerimeter(MyAwesomeShape shape)
            {
                return 666;
            }
        }
    }
}
