using NUnit.Framework;
using ShapeCalculator.Shapes;
using System;

namespace Shapes
{
    public class CircleTests
    {
        [Test]
        public void Ctr_WhenRadiusIsNegative_ShouldThrow()
        {
            // Arrange
            var r = -1;

            // Act & Assert
            Assert.Throws<ArgumentException>(() => new Circle(r));
        }
    }
}