using NUnit.Framework;
using ShapeCalculator.Shapes;
using System;

namespace Shapes
{
    public class TriangleTests
    {
        [Test]
        public void Ctr_WhenOneSideIsNegative_ShouldThrow()
        {
            // Arrange
            var a = 1;
            var b = 2;
            var c = 0;

            // Act & Assert
            Assert.Throws<ArgumentException>(() => new Triangle(a, b, c));
        }

        [Test]
        public void Ctr_WhenSumOfTwoSidesIsLessThanThird_ShouldThrow()
        {
            // Arrange
            var a = 1;
            var b = 2;
            var c = 10;

            // Act & Assert
            Assert.Throws<ArgumentException>(() => new Triangle(a, b, c));
        }
    }
}