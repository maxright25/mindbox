﻿using ShapeCalculator.Shapes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShapeCalculator.Calculators
{
    public class CircleCalculator : BaseCalculator<Circle>
    {
        private string ErrorMessage => "The circle must not be null!";
        protected override double GetArea(Circle shape) => shape != null 
            ? Math.PI * Math.Pow(shape.Radius, 2)
            : throw new ArgumentNullException(ErrorMessage);

        protected override double GetPerimeter(Circle shape) => shape != null 
            ? 2 * Math.PI * shape.Radius
            : throw new ArgumentNullException(ErrorMessage);
    }
}
