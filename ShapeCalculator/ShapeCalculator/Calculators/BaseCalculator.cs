﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapeCalculator.Calculators
{
    public abstract class BaseCalculator<T> : ICalculator where T : IShape
    {
        public double GetArea(IShape shape)
        {
            AssertType(shape);
            return GetArea((T)shape);
        }

        public double GetPerimeter(IShape shape)
        {
            AssertType(shape);
            return GetPerimeter((T)shape);
        }

        protected abstract double GetArea(T shape);
        protected abstract double GetPerimeter(T shape);

        private void AssertType(IShape shape)
        {
            if (!(shape is T))
                throw new ArgumentException($"{shape.GetType().Name} is not supported");
        }
    }
}
