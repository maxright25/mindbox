﻿using ShapeCalculator.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShapeCalculator.Calculators
{
    public class TriangleCalculator : BaseCalculator<Triangle>
    {
        protected override double GetArea(Triangle shape)
        {
            AssertNotNull(shape);
            var perimeter = GetPerimeter(shape);
            return Math.Sqrt(perimeter * (perimeter - shape.A) * (perimeter - shape.B) * (perimeter - shape.C));
        }

        protected override double GetPerimeter(Triangle shape)
        {
            AssertNotNull(shape);
            return (shape.A + shape.B + shape.C) / 2;
        }

        public bool IsRight(Triangle shape)
        {
            AssertNotNull(shape);
            var powered = new[] { shape.A, shape.B, shape.C }.OrderByDescending(x => x).Select(x => x * x).ToArray();
            return powered[0] == powered[1] + powered[2];
        }

        private void AssertNotNull(Triangle shape)
        {
            if (shape == null)
                throw new ArgumentNullException("The triangle must not be null");
        }
    }
}
