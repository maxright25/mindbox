﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShapeCalculator.Shapes
{
    public class Triangle : IShape
    {
        public Triangle(double a, double b, double c)
        {
            if (a <= 0 || b <= 0 || c <= 0)
                throw new ArgumentException("every side must be greater than zero");

            if (a + b <= c || a + c <= b || b + c <= a)
                throw new ArgumentException("the sum of every pair of two sides must be greater than the remaining side");

            A = a;
            B = b;
            C = c;
        }

        public double A { get; }
        public double B { get; }
        public double C { get; }
    }
}
