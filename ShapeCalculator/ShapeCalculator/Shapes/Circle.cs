﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapeCalculator.Shapes
{
    public class Circle : IShape
    {
        public Circle(double r)
        {
            if (r <= 0)
                throw new ArgumentException("r must be greater than zero");
            Radius = r;
        }

        public double Radius { get; }
    }
}
