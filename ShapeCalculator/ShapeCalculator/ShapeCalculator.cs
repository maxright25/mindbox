﻿using ShapeCalculator.Calculators;
using ShapeCalculator.Shapes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShapeCalculator
{
    public class ShapeCalculator : ICalculator
    {
        private readonly Dictionary<Type, ICalculator> _handlers;
        public ShapeCalculator()
        {
            _handlers = new Dictionary<Type, ICalculator>()
            {
                { typeof(Circle), new CircleCalculator() },
                { typeof(Triangle), new TriangleCalculator() }
            };
        }

        public double GetArea(IShape shape)
        {
            return GetHandler(shape).GetArea(shape);
        }

        public double GetPerimeter(IShape shape)
        {
            return GetHandler(shape).GetPerimeter(shape);
        }

        public void RegisterCustomHandler(Type type, ICalculator calculator)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            if (calculator == null)
                throw new ArgumentNullException(nameof(calculator));

            _handlers[type] = calculator;
        }

        private ICalculator GetHandler(IShape shape)
        {
            var type = shape.GetType();
            if (!_handlers.ContainsKey(type))
                throw new KeyNotFoundException($"The handler for {type.Name} is not registered");

            return _handlers[type];
        }
    }
}
