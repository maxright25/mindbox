﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapeCalculator
{
    public interface ICalculator
    {
        double GetArea(IShape shape);
        double GetPerimeter(IShape shape);
    }
}
