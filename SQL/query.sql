SELECT p.Name as ProductName, c.Name as CategoryName 
FROM [dbo].ProductToCategories ptc 
JOIN [dbo].Categories c on c.Id = ptc.CategoryId
RIGHT JOIN [dbo].Products p on p.Id = ptc.ProductId; 
