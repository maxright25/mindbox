DROP TABLE IF EXISTS [dbo].Products
DROP TABLE IF EXISTS [dbo].Categories
DROP TABLE IF EXISTS [dbo].ProductToCategories

CREATE TABLE [dbo].Products (
	Id INT NOT NULL Identity(1,1) PRIMARY KEY,
	Name varchar(max) NOT NULL
);

CREATE TABLE [dbo].Categories(
	Id INT NOT NULL Identity(1,1) PRIMARY KEY,
	Name varchar(max) NOT NULL
)

CREATE TABLE [dbo].ProductToCategories(
	ProductId INT NOT NULL,
	CategoryId INT NOT NULL,
CONSTRAINT FK_ProductToCategories_Products FOREIGN KEY (ProductId)     
    REFERENCES [dbo].Products (Id),
CONSTRAINT FK_ProductToCategories_Categories FOREIGN KEY (CategoryId)     
    REFERENCES [dbo].Categories (Id),
CONSTRAINT PK_ProductToCategories PRIMARY KEY NONCLUSTERED (ProductId, CategoryId),  
)