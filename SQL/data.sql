INSERT INTO [dbo].Products VALUES 
 ('Product1'),
 ('Product2'),
 ('Product3');

INSERT INTO [dbo].Categories VALUES
 ('Category1'),
 ('Category2'),
 ('Category3');

INSERT INTO [dbo].ProductToCategories VALUES
 (1, 1),
 (1, 2),
 (2, 2),
 (2, 3)
